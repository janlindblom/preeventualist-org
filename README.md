# Preeventualist.org

An implementation of "THE PREEVENTUALIST'S LOSING AND FINDING REGISTRY (a free service benefiting the ENLIGHTENED who have been LIGHTENED)".

#### Requirements

- Ruby
- Bundler
- PostgreSQL

#### Instructions

    $ bundle install
    $ rake db:create db:migrate
    $ bundle exec foreman start web

Then visit `localhost:3040/lost` and follow instructions.

### Credits!

All credits goes to \_why.

Inspiration: http://mislav.uniqpath.com/poignant-guide/book/chapter-6.html