class Router
  def self.setup(app)
    app.routes do
      add ["lost", "addfound"], Api::AddFound
      add ["lost", "addlost"], Api::AddLost
      add ["lost", "searchlost"], Api::SearchLost
      add ["lost", "searchfound"], Api::SearchFound
      add ["lost", "search"], Api::Search
      add ["lost"], Api::Lost
      add [:*], Api::Missing
    end
  end
end