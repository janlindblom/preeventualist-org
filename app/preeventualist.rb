require "api"
require "router"

Machine = Webmachine::Application.new do |app|
  Router.setup app

  app.configure do |config|
    config.adapter  = :Rack
  end
end
