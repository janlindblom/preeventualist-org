require 'uri'
require 'yaml'

require 'data_mapper'
require 'dm-migrations/migration_runner'

class Database
  def self.configuration
    unless ENV['DATABASE_URL']
      yaml = YAML.load_file(File.expand_path("../../config/database.yml", __FILE__))
      config = yaml["#{ENV['RACK_ENV']}"]
      {
        adapter:  ENV['DB_ADAPTER']   || config['adapter'],
        host:     ENV['DB_HOST']      || config['host'],
        charset:  ENV['DB_CHARSET']   || config['charset'],
        port:     ENV['DB_PORT']      || config['port'],
        pool:     ENV['DB_POOL']      || config['pool'],
        user:     ENV['DB_USER']      || config['user'],
        password: ENV['DB_PASSWORD']  || config['password'],
        database: ENV['DB_DATABASE']  || config['database']
      }
    else
      db = URI(ENV['DATABASE_URL'])
        {
          adapter: db.scheme,
          host: db.host,
          port: db.port,
          user: db.user,
          password: db.password,
          database: db.path.gsub('/','')
        }
    end

  end

  def self.configure!
    @config = Database.configuration
    DataMapper.setup(:default, @config)
  end

end