module Models
  class Item
    attr_accessor :name, :item, :type, :desc, :at, :seen
    
    def initialize(args)
      @name = args[:name] if args.has_key? :name
      @item = args[:item] if args.has_key? :item
      @desc = args[:desc] if args.has_key? :desc
      @at = args[:at] if args.has_key? :at
      @seen = args[:seen] if args.has_key? :seen
      @type = args[:type] if args.has_key? :type
    end
    
    def where
      if @type == "found"
        @at
      elsif @type == "lost"
        @seen
      end
    end

  end

end