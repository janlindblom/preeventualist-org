module Models
  class Lost
    include DataMapper::Resource
    property :id, Serial
    property :name, String, length: 100
    property :item, String, length: 100
    property :seen, String, length: 100
    property :desc, Text
    timestamps :at

    def to_item
      Item.new(name: self.name, item: self.item, seen: self.seen, type: "lost", desc: self.desc)
    end
  end
end
