module Models
  class Found
    include DataMapper::Resource
    property :id, Serial
    property :name, String, length: 100
    property :item, String, length: 100
    property :at, String, length: 100
    property :desc, Text
    timestamps :at

    def to_item
      Item.new(name: self.name, item: self.item, at: self.at, type: "found", desc: self.desc)
    end
  end
end