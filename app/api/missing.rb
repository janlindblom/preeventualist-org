require 'uri'

module Api
  class Missing < Webmachine::Resource

    def moved_permanently?
      URI.parse(request.base_uri.to_s + "lost")
    end

  end
end
