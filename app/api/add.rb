require "uri"
require "models/found"
require "models/lost"

module Api
  class AddFound < Webmachine::Resource
    def allows_methods
      ["GET"]
    end

    def content_types_provided
      [["text/plain", :to_s]]
    end

    def encodings_provided
      {
        "gzip" => "encode_gzip",
        "deflate" => "encode_deflate",
        "identity" => "encode_identity"
      }
    end

    def malformed_request?
      return true unless request.query.has_key?("name")
      return true unless request.query.has_key?("item")
      return true unless request.query.has_key?("at")
    end

    def to_s
      query = request.query
      name = URI.unescape(query["name"])
      item = URI.unescape(query["item"])
      at = URI.unescape(query["at"])
      desc = URI.unescape(query["desc"]) if query.has_key?("desc")
      found = Models::Found.new({name: name, item: item, at: at, desc: desc})
      return 400 unless found.save

      output = ["status,item,at,name,desc"]
      output << ["ok", found.item, found.at, found.name, found.desc].join(",")
      
      output.join("\n")
    end

  end

  class AddLost < Webmachine::Resource
    def allows_methods
      ["GET"]
    end

    def content_types_provided
      [["text/plain", :to_s]]
    end

    def encodings_provided
      {
        "gzip" => "encode_gzip",
        "deflate" => "encode_deflate",
        "identity" => "encode_identity"
      }
    end

    def malformed_request?
      return true unless request.query.has_key?("name")
      return true unless request.query.has_key?("item")
      return true unless request.query.has_key?("seen")
    end

    def to_s
      query = request.query
      name = URI.unescape(query["name"])
      item = URI.unescape(query["item"])
      seen = URI.unescape(query["seen"])
      desc = URI.unescape(query["desc"]) if query.has_key?("desc")
      lost = Models::Lost.new({name: name, item: item, seen: seen, desc: desc})
      return 400 unless lost.save

      output = ["status,item,at,name,desc"]
      output << ["ok", lost.item, lost.seen, lost.name, lost.desc].join(",")
      
      output.join("\n")
    end

  end

end
