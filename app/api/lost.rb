require 'digest/sha1'
require "liquid"
require "important_news"
require "celluloid"

module Api
  class Lost < Webmachine::Resource
    def allows_methods
      ["HEAD", "GET"]
    end

    def content_types_provided
      [["text/plain", :to_s]]
    end

    def languages_provided
      ["en"]
    end

    def generate_etag
      Digest::SHA1.hexdigest(generate_output)
    end

    def expires
      Time.now + 3600
    end

    def options
      {"Cache-Control" => "max-age=#{1.hour.seconds.to_i}"}
    end

    def to_s
      generate_output
    end

    private

    def generate_output
      files = ["lost.txt", "about.txt", "news.txt", "searching.txt", "using.txt"]
      output = ""
      files.each do |fname|
        output += Celluloid::Actor[:fetchers].get(fname)
      end
      output
    end

  end
end
