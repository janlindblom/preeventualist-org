require "uri"
require "JSON"
require 'digest/sha1'
require "models/found"
require "models/lost"
require "models/item"

module Api
class Search < Webmachine::Resource
    def allows_methods
      ["HEAD", "GET"]
    end

    def content_types_provided
      [["text/plain", :to_s]]
    end

    def encodings_provided
      {
        "gzip" => "encode_gzip",
        "deflate" => "encode_deflate",
        "identity" => "encode_identity"
      }
    end

    def languages_provided
      ["en"]
    end

    def malformed_request?
      request.query.has_key?("q") ? false : true
    end

    def to_s
      query = request.query["q"]
      items = search(query)
      
      Api::prepare_output(items)
    end

    def generate_etag
      items = search(request.query["q"])
      Digest::SHA1.hexdigest(items.to_json)
    end

    private

    def search(query)
      lost_items = Models::Lost.all(:item.like => query).map { |x| x.to_item }
      found_items = Models::Found.all(:item.like => query).map { |x| x.to_item }
      items = lost_items + found_items
      items
    end
  end

  class SearchFound < Webmachine::Resource
    def allows_methods
      ["HEAD", "GET"]
    end

    def content_types_provided
      [["text/plain", :to_s]]
    end

    def encodings_provided
      {
        "gzip" => "encode_gzip",
        "deflate" => "encode_deflate",
        "identity" => "encode_identity"
      }
    end

    def languages_provided
      ["en"]
    end

    def malformed_request?
      request.query.has_key?("q") ? false : true
    end

    def to_s
      query = request.query["q"]
      found_items = search(query)
      
      Api::prepare_output(found_items)
    end

    def generate_etag
      items = search(request.query["q"])
      Digest::SHA1.hexdigest(items.to_json)
    end

    private

    def search(query)
      found_items = Models::Found.all(:item.like => query).map { |x| x.to_item }
      found_items
    end

  end

  class SearchLost < Webmachine::Resource
    def allows_methods
      ["HEAD", "GET"]
    end

    def content_types_provided
      [["text/plain", :to_s]]
    end

    def encodings_provided
      {
        "gzip" => "encode_gzip",
        "deflate" => "encode_deflate",
        "identity" => "encode_identity"
      }
    end

    def languages_provided
      ["en"]
    end

    def malformed_request?
      request.query.has_key?("q") ? false : true
    end

    def to_s
      query = request.query["q"]
      lost_items = search(query)
      
      Api::prepare_output(lost_items)
    end

    def generate_etag
      items = search(request.query["q"])
      Digest::SHA1.hexdigest(items.to_json)
    end

    private

    def search(query)
      lost_items = Models::Lost.all(:item.like => query).map { |x| x.to_item }
      lost_items
    end

  end

  def self.prepare_output(items)
    return "no results" if items.empty?
    output = []
    items.each do |item|
      itementry = ["#{item.type.capitalize} item: #{item.item}"]
      itementry << "#{item.type.capitalize} by: #{item.name}"
      itementry << "Location: #{item.where}"
      itementry << "Description:"
      itementry << item.desc + "\n"
      output << itementry.join("\n")
    end
    (("-" * 80) + "\n") + output.join(("-" * 80) + "\n")
  end

end
