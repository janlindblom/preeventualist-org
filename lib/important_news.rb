require "liquid"

class ImportantNews < Liquid::Tag

  def render(context)
    dir = File.expand_path("../../updates", __FILE__)
    files = Dir[File.join(dir, "*.txt")]
    updates = []
    files.reverse.each do |file|
      dateparts = File.basename(file).split(".")[0].split("-")
      update = "/ #{dateparts[2]} #{to_month_name(dateparts[1])} #{dateparts[0]} /\n"
      update += File.read(file)
      updates << update
    end
    updates.join "\n"
  end

  private

  def to_month_name(month_num)
    month = month_num.to_i
    case month
    when 1
      "Jan"
    when 2
      "Feb"
    when 3
      "March"
    when 4
      "April"
    when 5
      "May"
    when 6
      "June"
    when 7
      "July"
    when 8
      "Aug"
    when 9
      "Sep"
    when 10
      "Oct"
    when 11
      "Nov"
    when 12
      "Dec"
    else
      month_num
    end
  end
end

Liquid::Template.register_tag('important_news', ImportantNews)
