require 'celluloid/current'
require "liquid"

class FileFetcher
  include Celluloid

  def get(filename)
    file = File.join(File.expand_path("../../files", __FILE__), filename)
    template = File.read(file)
    output = Liquid::Template.parse(template).render + "\n"

    output
  end
end
