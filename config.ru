#\ -s puma
$LOAD_PATH.unshift(File.expand_path("../lib", __FILE__))
$LOAD_PATH.unshift(File.expand_path("../app", __FILE__))

require "rubygems"
require "bundler/setup"

require "dotenv"
Dotenv.load

require "webmachine"
require "webmachine/adapters/rack"

require 'celluloid/current'
Celluloid.task_class = Celluloid::Task::Threaded
require "file_fetcher"

require 'logger'

require 'dm-core'
require 'dm-types'
require 'dm-validations'

require "database"
require "preeventualist"

Database.configure!

#class TaskGroup < Celluloid::SupervisionGroup
#  pool FileFetcher, as: :fetchers, size: 5
#end

require 'celluloid/supervision/supervise'
class TaskGroup < Celluloid::Supervision::Container
  pool FileFetcher, as: :fetchers, size: 4
end

TaskGroup.run!

run Machine.adapter